/**
 * Specify required object
 *
 * @examples require(".").sampleData
 */
export interface IModel {
  name: string;
  projectName: string;
  month: string;
  year: string;
}

export const sampleData: IModel[] = [
  {
    name: 'Jane',
    projectName: 'Cool project',
    month: 'May',
    year: '2022',
  },
  {
    name: 'John',
    projectName: 'Awesome project',
    month: 'June',
    year: '2022',
  },
];
